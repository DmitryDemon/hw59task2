import React, { Component } from 'react';
import './App.css';
import AddJoke from "./components/AddJoke/AddJoke";
import Button from "./components/AddJoke/Button/Button";

class App extends Component {

    state = {
        jokes : [],
    };

    addJokeHendler = () => {
        fetch('https://api.chucknorris.io/jokes/random').then(response => {
            if (response.ok) {
                return response.json()
            }

            throw new Error('Something wrong with request');
        }).then(jokes => {

            this.setState({jokes: jokes.value});
        }).catch(error => {
            console.log(error);
        });
    };


    componentDidMount() {

        this.addJokeHendler();
    }


  render() {
    return (
      <div className="App">
        <AddJoke joke={this.state.jokes}/>
          <Button newJoke={()=>this.addJokeHendler()}/>
      </div>
    );
  }
}

export default App;
